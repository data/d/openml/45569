# OpenML dataset: DBLP-QuAD

https://www.openml.org/d/45569

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

DBLP-QuAD is a scholarly question answering dataset over the DBLP knowledge graph. The dataset can also be found at https://zenodo.org/record/7643971 and https://huggingface.co/datasets/awalesushil/DBLP-QuAD. The paper can be found at https://arxiv.org/abs/2303.13351. The reference DBLP KG dump in .nt format can be found at https://zenodo.org/record/7638511.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45569) of an [OpenML dataset](https://www.openml.org/d/45569). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45569/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45569/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45569/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

